const express = require('express')
const bodyParser = require('body-parser')
const app = express()
var pg = require('pg');
var conString = "postgres://postgres:root@localhost/automobiDB";

app.listen(3000);
app.use(express.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods","POST, GET, PUT, DELETE, OPTIONS");
    next();
});

var client = new pg.Client(conString);

client.connect(function (err) {
    if (err) throw err;

    console.log("connected");

});


// routes VERSIONS
app.get('/version', function(req, res) {

        client.query('SELECT * FROM Version', function (err, result) {
            if (err) {
                console.log(err);
            }
            // console.log("Tamanho == "+result.rows.length);
            console.log("my versions === "+JSON.stringify(result.rows));

            res.send(JSON.stringify(result.rows))
        });

});

app.post('/version',function (req, res, next) {

    var params =[req.body.version];

    client.query('INSERT INTO Version (version) VALUES ($1) RETURNING id_version',params, function (err, result) {
        if (err) {
            res.send(err);
        }
         res.send(JSON.stringify(result.rows))
    });

});

app.delete('/version/:id',function (req, res, next) {

    var params =[req.params.id];
    client.query('DELETE FROM version WHERE id_version = ($1)',params, function (err, result) {
        if (err) {
            res.sendStatus(err);
        }
        console.log("DELETE SUCCESS");
        console.log("total results === "+JSON.stringify(result.rowCount));
        //
        res.jsonp(result.rowCount);
    });

});

app.put('/version',function (req, res, next) {

    var params =[req.body.id_version,req.body.version];
    client.query('UPDATE version SET version = ($2) WHERE id_version = ($1)',params, function (err, result) {
        if (err) {
            res.sendStatus(err);
        }

        res.jsonp(result.rowCount);
    });

});


//Routes BRANDS

app.post('/brand', function (req, res, next) {

     var params =[req.body['name']];

     client.query('INSERT INTO Brand (name) VALUES ($1) RETURNING id_brand ',params, function (err, result) {
        if (err) {
            res.send(err);
        }
         res.jsonp(result.rows.id_brand);
    });

});

app.get('/brands', function(req, res) {

    client.query('SELECT * FROM Brand', function (err, result) {
        if (err) {
            console.log(err);
        }

        res.jsonp(result.rows);
    });
});

app.delete('/brand/:id',function (req, res, next) {

    var params =[req.params.id];
    client.query('DELETE FROM brand WHERE id_brand= ($1)',params, function (err, result) {
        if (err) {
            res.sendStatus(err);
        }

        res.jsonp(result.rowCount);
    });

});

app.put('/brand',function (req, res, next) {

    var params =[req.body.id_brand,req.body.name];
    client.query('UPDATE brand SET name = ($2) WHERE id_brand = ($1)',params, function (err, result) {
        if (err) {
            res.sendStatus(err);
        }
        res.jsonp(result.rowCount);
    });

});



//Routew YEARS



app.get('/years', function(req, res) {

    client.query('SELECT * FROM modelyear', function (err, result) {
        if (err) {
            res.send(err);
        }
        res.send(JSON.stringify(result.rows))
    });

});


app.post('/year',function (req, res, next) {


    var params =[req.body.model_year_id];

    client.query('INSERT INTO ModelYear (year) VALUES ($1) RETURNING model_year_id',params, function (err, result) {
        if (err) {
            res.send(err);
        }
        res.jsonp(result.row.model_year_id);
    });

});


app.delete('/year/:id',function (req, res, next) {

    var params =[req.params.id];

    client.query('DELETE FROM ModelYear WHERE model_year_id= ($1)',params, function (err, result) {
        if (err) {
            res.sendStatus(err);
        }
        res.jsonp(result.rowCount);
    });

});

app.put('/year',function (req, res, next) {

    var params =[req.body.model_year_id,req.body.year];

    client.query('UPDATE ModelYear SET year = ($2) WHERE model_year_id = ($1)',params, function (err, result) {
        if (err) {
            res.sendStatus(err);
        }
        res.jsonp(result.rowCount);
    });

});



// routes MODELS
app.get('/models',function(req, res) {

    client.query('SELECT m.modelid, m.name as model, b.name as brand,v.version, my.year \n' +
        '        FROM model m, brand b, version v, modelyear my \n' +
        '        WHERE m.id_brand = b.id_brand AND m.id_version = v.id_version AND my.model_year_id = m.model_year_id ORDER BY modelid ASC;', function (err, result) {
        if (err) {
            console.log(err);
        }

        res.jsonp(result.rows);
    });

});


app.post('/model',function (req, res, next) {

    var params =[req.body.modelid,
        req.body.id_brand,
        req.body.name,
        req.body.model_year_id,
        req.body.id_version];

    client.query('INSERT INTO Model (id_brand, name, model_year_id, id_version) VALUES ($2,$3,$4,$5) RETURNING modelid',params, function (err, result) {
        if (err) {
            res.send(err);
        }
        res.sendStatus(200);
    });

});


app.delete('/model/:id',function (req, res, next) {


    var params =[req.params.id];

    client.query('DELETE FROM Model WHERE modelid = ($1)',params, function (err, result) {
        if (err) {

            res.sendStatus(err);
        }
        res.jsonp(result.rowCount);
    });

});

app.put('/model',function (req, res, next) {

    var params =[req.body.modelid,
        req.body.id_brand,
        req.body.name,
        req.body.model_year_id,
        req.body.id_version];

    client.query('UPDATE Model SET name = ($3),id_brand = ($2), model_year_id = ($4), id_version = ($5)' +
        'WHERE modelid = ($1)',params, function (err, result) {
        if (err) {
            res.sendStatus(err);
        }
        res.jsonp(result.rowCount);
    });

});



module.exports = app;
