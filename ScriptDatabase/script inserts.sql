﻿--INSERTO TO TESTS

INSERT INTO version (version) VALUES ('1.0 rally');
INSERT INTO version (version) VALUES ('hatch');
INSERT INTO version (version) VALUES ('sedan');
INSERT INTO version (version) VALUES ('teste') RETURNING id_version;


INSERT INTO brand (name) VALUES ('Ford');
INSERT INTO brand (name) VALUES ('wolksvagen');
INSERT INTO brand (name) VALUES ('honda');
INSERT INTO brand (name) VALUES ('chevtolet');
INSERT INTO brand (name) VALUES ('fiat');


INSERT INTO modelyear (year) VALUES (1947);
INSERT INTO modelyear (year) VALUES (1948);
INSERT INTO modelyear (year) VALUES (1949);
INSERT INTO modelyear (year) VALUES (1950);


INSERT INTO model (id_brand,name, model_year_id, id_version) VALUES (2,'gol',3,1);
INSERT INTO model (id_brand,name, model_year_id, id_version) VALUES (3,'brasilia',2,3);
INSERT INTO model (id_brand,name, model_year_id, id_version) VALUES (1,'gol',1,2);
INSERT INTO model (id_brand,name, model_year_id, id_version) VALUES (3,'chevett',1,2);
INSERT INTO model (id_brand,name, model_year_id, id_version) VALUES (2,'celta',3,1);
INSERT INTO model (id_brand,name, model_year_id, id_version) VALUES (4,'palio',4,2);
INSERT INTO model (id_brand,name, model_year_id, id_version) VALUES (3,'teste',2,3);
INSERT INTO model (id_brand,name, model_year_id, id_version) VALUES (2,'civic',3,3);
INSERT INTO model (id_brand,name, model_year_id, id_version) VALUES (1,'corola',1,4);
