﻿CREATE TABLE Version (id_version SERIAL PRIMARY KEY ,
	version VARCHAR (255) NOT NULL); 
        
CREATE TABLE ModelYear (model_year_id  SERIAL PRIMARY KEY ,
        year int NOT NULL); 
        
CREATE TABLE Brand (id_brand SERIAL PRIMARY KEY ,
        name VARCHAR (255) NOT NULL); 
        
CREATE TABLE Model (modelid SERIAL PRIMARY KEY ,
         id_brand int NOT NULL,name VARCHAR (255)NOT NULL,
         model_year_id BIGINT NOT NULL,
         id_version int NOT NULL ); 
        
        
        ALTER TABLE Model ADD CONSTRAINT version_model_fk
        FOREIGN KEY (id_version)
        REFERENCES Version (id_version)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION;
        
        ALTER TABLE Model ADD CONSTRAINT modelyear_model_fk
        FOREIGN KEY (model_year_id)
        REFERENCES ModelYear (model_year_id)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION;
        
        ALTER TABLE Model ADD CONSTRAINT brand_model_fk
        FOREIGN KEY (id_brand)
        REFERENCES Brand (id_brand)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION;
        
 ALTER TABLE Version ADD CONSTRAINT version_unique UNIQUE (version); 
 ALTER TABLE ModelYear ADD CONSTRAINT year_unique UNIQUE (year); 
 ALTER TABLE Brand ADD CONSTRAINT name_unique UNIQUE (name); 

--insert values to test

﻿INSERT INTO version (version) VALUES ('1.0 rally');
INSERT INTO version (version) VALUES ('hatch');
INSERT INTO version (version) VALUES ('sedan');
INSERT INTO version (version) VALUES ('teste') RETURNING id_version;


INSERT INTO brand (name) VALUES ('Ford');
INSERT INTO brand (name) VALUES ('wolksvagen');
INSERT INTO brand (name) VALUES ('honda');
INSERT INTO brand (name) VALUES ('chevtolet');
INSERT INTO brand (name) VALUES ('fiat');


INSERT INTO modelyear (year) VALUES (1947);
INSERT INTO modelyear (year) VALUES (1948);
INSERT INTO modelyear (year) VALUES (1949);
INSERT INTO modelyear (year) VALUES (1950);


INSERT INTO model (id_brand,name, model_year_id, id_version) VALUES (2,'gol',3,1);
INSERT INTO model (id_brand,name, model_year_id, id_version) VALUES (3,'brasilia',2,3);
INSERT INTO model (id_brand,name, model_year_id, id_version) VALUES (1,'gol',1,2);
INSERT INTO model (id_brand,name, model_year_id, id_version) VALUES (3,'chevett',1,2);
INSERT INTO model (id_brand,name, model_year_id, id_version) VALUES (2,'celta',3,1);
INSERT INTO model (id_brand,name, model_year_id, id_version) VALUES (4,'palio',4,2);
INSERT INTO model (id_brand,name, model_year_id, id_version) VALUES (3,'teste',2,3);
INSERT INTO model (id_brand,name, model_year_id, id_version) VALUES (2,'civic',3,3);
INSERT INTO model (id_brand,name, model_year_id, id_version) VALUES (1,'corola',1,4);


